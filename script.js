const OTHER_THRESHOLD = 1;
const worldPopulation = data.map(element => element.population).reduce((a, b) => a + b, 0);

const allCountries = data.map(element => {
    return {
        description: element.country,
        percentage: (element.population / worldPopulation) * 100,
        value: element.population
    };
});

allCountries.sort((a, b) => b.percentage - a.percentage);

const countriesBigPopulation = allCountries.filter(element => element.percentage >= OTHER_THRESHOLD);

const countriesSmallPopulation = allCountries.filter(element => element.percentage < OTHER_THRESHOLD);

const list = countriesSmallPopulation.length >= 2 ? [...countriesBigPopulation, ...getOtherCountries()] : [...allCountries];

function getOtherCountries() {
    return [{
        description: 'Other',
        percentage: countriesSmallPopulation.map(element => element.percentage).reduce((a, b) => a + b, 0),
        value: countriesSmallPopulation.map(element => element.value).reduce((a, b) => a + b, 0)
    }];
}

const canvas = document.getElementById('canvas');
canvas.width = 750;
canvas.height = 500;

const ctx = canvas.getContext('2d');
const colors = CSS_COLOR_NAMES.slice(0);

let startAngle = 0;
list.forEach(({percentage, description}, index, list) => {
    // sector
    ctx.beginPath();
    ctx.fillStyle = colors.splice(Math.round(Math.random() * (colors.length - 1)), 1)[0];
    ctx.moveTo(250, 250);
    ctx.arc(250, 250, 200, startAngle, startAngle -= percentage * Math.PI / 50, true);
    ctx.lineTo(250, 250);
    ctx.fill();

    // legend
    const lHeight = 500 / list.length;
    ctx.fillRect(500, lHeight * index + (lHeight - 15) / 2, 15, 15);
    ctx.fillStyle = '#000';
    ctx.fillText(`${description} ( ${percentage.toFixed(2)}%)`, 520, lHeight * index + (lHeight - 15) / 2 + 10);
});
